# Site.asqatasun.ovh

Content of Site.asqatasun.ovh, used for Quality Assurance of Asqatasun

## Content

Content of the website is in `public/` directory.

The following files will be denied access to crawlers (but can be seen from a regular browser):

```shell script
/page-access-forbidden-for-robots.html
/folder-test/page-access-forbidden-for-robots.html
/folder-test2/page-access-forbidden-for-robots.html
```

## Old tests

Tests that were done previously can be found in Asqatasun source code :

* <https://gitlab.com/asqatasun/Asqatasun/-/tree/master/engine/crawler/src/test/resources/sites>

## Robots.txt

Infomaniak hosting imposes to have an empty robots.txt file to remove the default one
See <https://www.infomaniak.com/fr/support/faq/2136/fichier-robotstxt-cree-par-defaut>
